package com.example.viper.userlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class ListViewAdapter extends ArrayAdapter<UsersDetails> {
    private Context context;
    static ArrayList<UsersDetails> usersDetailsArrayList;


    public ListViewAdapter(@NonNull Context context, ArrayList<UsersDetails> usersDetailsArrayList) {
        super(context, R.layout.result_layout, usersDetailsArrayList);
        this.context = context;
        this.usersDetailsArrayList = usersDetailsArrayList;
    }


    class ViewHolder {
        TextView firstNameTextView;
        TextView lastNameTextView;
        TextView phoneTextView;

    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.result_layout, parent, false);
            holder.firstNameTextView = convertView.findViewById(R.id.firstName);
            holder.lastNameTextView = convertView.findViewById(R.id.lastName);
            holder.phoneTextView = convertView.findViewById(R.id.phone);


            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.firstNameTextView.setText("first name: "+usersDetailsArrayList.get(position).getFirstName());
        holder.lastNameTextView.setText("last name: "+usersDetailsArrayList.get(position).getLastName());
        holder.phoneTextView.setText("phone: "+usersDetailsArrayList.get(position).getMobile());


        return convertView;
    }
}
