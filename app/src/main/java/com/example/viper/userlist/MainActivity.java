package com.example.viper.userlist;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    //URL for fetching data from API
    String JsonURL = "http://dropbox.sandbox2000.com/intrvw/users.json";
    private final String BaseURL = "http://dropbox.sandbox2000.com/";

    private JsonServiceAPI jsonServiceAPI;
    private ArrayList<UsersDetails>usersDetailsArrayList = new ArrayList<UsersDetails>();
    private ListView userListView;
    private ListViewAdapter listViewAdapter;
    private ProgressDialog mProgress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
            initialize Retrofit
         */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonServiceAPI = retrofit.create(JsonServiceAPI.class);
        usersDetailsArrayList=new ArrayList<>();
        userListView=findViewById(R.id.userDetailsShow);


        //showing PROGRESS BAR
        mProgress = new ProgressDialog(MainActivity.this);
        mProgress.setMessage("getting data...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        //Parsing Json

        mProgress.show();

        Call<JsonResponse>arrayListCall=
                jsonServiceAPI.getJsonResponse();

        arrayListCall.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                if(response.code()==200)
                {
                    //Get respone from API
                    JsonResponse resource =response.body();
                    //Toast.makeText(MainActivity.this,"Data loading",Toast.LENGTH_SHORT).show();

                    //Seperate data from API

                    for (int i = 0; i < resource.getUsers().size(); i++) {
                        int id = resource.getUsers().get(i).getId();
                        String firstName = resource.getUsers().get(i).getFirstName();
                        String lastName = resource.getUsers().get(i).getLastName();
                        String mobile = resource.getUsers().get(i).getPhones().getMobile();



                        usersDetailsArrayList.add(new UsersDetails(id,firstName,lastName,mobile));
                    }

                    listViewAdapter=new ListViewAdapter(MainActivity.this,usersDetailsArrayList);
                    userListView.setAdapter(listViewAdapter);
                    mProgress.dismiss();


                }
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Fail",Toast.LENGTH_SHORT).show();

            }
        });
    }
}
