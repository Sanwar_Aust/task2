package com.example.viper.userlist;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonServiceAPI {
    @GET("intrvw/users.json")
    Call<JsonResponse> getJsonResponse();

}
